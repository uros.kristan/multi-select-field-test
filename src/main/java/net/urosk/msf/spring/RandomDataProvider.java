package net.urosk.msf.spring;

import com.vaadin.flow.data.provider.AbstractBackEndDataProvider;
import com.vaadin.flow.data.provider.Query;

import java.util.stream.Stream;

public class RandomDataProvider extends AbstractBackEndDataProvider<DataBean, String> {

    private RandomDataService randomDataService;

    public RandomDataProvider(RandomDataService randomDataService) {
        this.randomDataService = randomDataService;
    }

    @Override
    protected Stream<DataBean> fetchFromBackEnd(Query<DataBean, String> query) {
        return randomDataService.getData()
                .stream()
                .skip(query.getOffset())
                .limit(query.getLimit());
    }

    @Override
    protected int sizeInBackEnd(Query<DataBean, String> query) {
        return randomDataService.getData().size();
    }
}
