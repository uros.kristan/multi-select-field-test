package net.urosk.msf.spring;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class RandomDataService implements Serializable {
    List<DataBean> list = new ArrayList<>();

    @PostConstruct
    void init() {

        for (int i = 0; i < 1000; i++) {
            list.add(new DataBean(i, RandomStringUtils.randomAlphabetic(20)));

        }

    }

    public List<DataBean> getData() {
        return list;
    }
}
