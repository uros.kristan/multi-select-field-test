package net.urosk.msf.spring;

public class DataBean {

    @Override
    public String toString() {
        return "DataBean{" +
                "id=" + id +
                ", msg='" + msg + '\'' +
                '}';
    }

    public DataBean(int id, String msg) {
        this.id = id;
        this.msg = msg;
    }

    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    private String msg;

}
