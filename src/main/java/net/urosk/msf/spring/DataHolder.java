package net.urosk.msf.spring;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DataHolder implements Serializable {

    private String name;
    private Set<DataBean> dataBeans = new HashSet<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<DataBean> getDataBeans() {
        return dataBeans;
    }

    public void setDataBeans(Set<DataBean> dataBeans) {
        this.dataBeans = dataBeans;
    }

    @Override
    public String toString() {
        return "Size: " +dataBeans.size()+", DataHolder{" +
                "name='" + name + '\'' +
                ", dataBeans=" + dataBeans +
                '}';
    }
}
