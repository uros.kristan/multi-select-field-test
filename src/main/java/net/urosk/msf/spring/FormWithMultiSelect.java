package net.urosk.msf.spring;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.provider.CallbackDataProvider;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.provider.Query;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.PWA;
import org.springframework.context.annotation.Scope;
import org.vaadin.gatanaso.MultiselectComboBox;

import java.awt.*;
import java.util.stream.Stream;

@Route(layout = MainAppLayout.class)
public class FormWithMultiSelect extends VerticalLayout {

    public FormWithMultiSelect(@Autowired RandomDataService randomDataService) {

        Binder<DataHolder> binder = new Binder<>();

        MultiselectComboBox<DataBean> multiSelectComboBox = new MultiselectComboBox<>();
        multiSelectComboBox.setLabel("The best multiselect ComboBox");
        multiSelectComboBox.setCompactMode(false);
        multiSelectComboBox.setItemLabelGenerator(DataBean::getMsg);
        multiSelectComboBox.setDataProvider(new RandomDataProvider(randomDataService));

        binder.forField(multiSelectComboBox).bind(DataHolder::getDataBeans, DataHolder::setDataBeans);

        TextField textField = new TextField();
        binder.forField(textField).bind(DataHolder::getName, DataHolder::setName);

        FormLayout formLayout = new FormLayout();
        formLayout.setWidth("300px");
        formLayout.add(new H2("The great Multi Select Combobox"));
        formLayout.add(multiSelectComboBox);

        formLayout.add(textField);

        add(formLayout);

        Button saveButton = new Button("Save",
                event -> {
                    try {

                        DataHolder dataHolder = new DataHolder();
                        binder.writeBean(dataHolder);
                        Notification.show(dataHolder.toString(), 1000, Notification.Position.TOP_CENTER);


                        binder.setBean(dataHolder);

                    } catch (ValidationException e) {

                        Notification.show(e.toString(), 1000, Notification.Position.TOP_CENTER);
                    }
                });

        add(saveButton);
    }

}
