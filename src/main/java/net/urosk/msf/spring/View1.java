package net.urosk.msf.spring;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.gatanaso.MultiselectComboBox;

@Route(value = "", layout = MainAppLayout.class)
public class View1 extends VerticalLayout {

    public View1() {

        add(new Button("Go"));
    }

}
